const cardTemplate = (card) => `
    <div id="card-${card.deck}-${card.hanzi}" class="card">
    <div class="card-header">
    <p class="card-pinyin">${card.pinyin}</p>
    <p class="card-difficulty">${card.difficulty}</p>
    </div> 
    <p class="card-hanzi">${card.hanzi}</p>
    <p class="card-deck">${card.deck}</p>
    <div class="card-details">
        <p class="card-english">${card.english}</p>
        <p class="card-due">Next due ${
          Date.parse(card.due) - Date.parse(new Date()) > 0
            ? "in "(Date.parse(card.due) - Date.parse(new Date())).toString() +
              " days"
            : "today"
        }</p>
    </div>
    <span class="card-stability">${card.stability}</span>
    </div>
`;

const renderCards = () => {
  var allCards = CardManager.getAllCards();
  var cardHTML = allCards
    .map((card) => {
      var card = cardTemplate(card);
      return card;
    })
    .join("");
  $("#cardContainer").html(cardHTML);
};

CardManager.onFsrsReady(renderCards);
