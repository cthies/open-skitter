var width =
  window.innerWidth ||
  document.documentElement.clientWidth ||
  document.body.clientWidth;

var writerSize = width > 1000 ? 300 : 700;
$("#option-row").width(writerSize);
$("#hide-button").hide();
$("#mute-button").hide();
$("#complete-message").hide();

var currentCard;
var writer;
var word;
var wordMistakes = 0;

var muted = true;
var audio;

const onMistake = (strokeData) => {};
const onCorrectStroke = (strokeData) => {};
const onWordComplete = (summaryData) => {
  CardManager.saveCardReview(
    currentCard,
    summaryData.totalMistakes + wordMistakes
  );
  if (!muted) {
    audio.play();
  }
  console.log($("#progress-filled"));
  console.log(initialCardsDue);
  console.log(dueCards.length);
  console.log(
    (
      ((initialCardsDue - dueCards.length + 1) * 100) /
      initialCardsDue
    ).toString() + "%"
  );
  $("#progress-filled").css(
    "width",
    (
      ((initialCardsDue - dueCards.length + 1) * 100) /
      initialCardsDue
    ).toString() + "%"
  );
  setTimeout(updateHanziWriter, 1000);
};
const onCharacterComplete = (summaryData) => {
  word = word.substring(1);
  wordMistakes += summaryData.totalMistakes;
  setTimeout(() => {
    writer.setCharacter(word[0]);
    startQuiz();
  }, 1000);
};

const startQuiz = () => {
  if (!word) {
    return;
  }
  if (word.length == 1) {
    writer.quiz({
      onMistake: onMistake,
      onCorrectStroke: onCorrectStroke,
      onComplete: onWordComplete,
      highlightOnComplete: true,
    });
  } else {
    writer.quiz({
      onMistake: onMistake,
      onCorrectStroke: onCorrectStroke,
      onComplete: onCharacterComplete,
      highlightOnComplete: true,
    });
  }
};

var initialCardsDue;
var dueCards;

const updateHanziWriter = () => {
  wordMistakes = 0;
  dueCards = CardManager.getDueCards(20);
  if (!initialCardsDue) {
    initialCardsDue = dueCards.length;
  }
  $("#cards-done").text(initialCardsDue - dueCards.length + 1);
  $("#cards-total").text(initialCardsDue);
  if (dueCards.length == 0) {
    $("#quiz-container").hide();
    $("#complete-message").show();
    return;
  }
  currentCard = dueCards[0];
  document.getElementById("pinyin").innerHTML = currentCard.pinyin;
  document.getElementById("english").innerHTML = currentCard.english;
  word = currentCard.hanzi;
  var character;
  if (word.length == 1) {
    character = word;
  } else {
    character = word[0];
  }
  if (!writer) {
    writer = HanziWriter.create("character-target-div", character, {
      charDataLoader: function (char, onComplete) {
        $.getJSON(
          "https://raw.githubusercontent.com/chanind/hanzi-writer-data/master/data/" +
            char +
            ".json",
          function (allData) {
            $("#log").text("retrieved hanzi data");
            onComplete(allData);
          }
        );
      },
      width: writerSize,
      height: writerSize,
      padding: 5,
      showCharacter: false,
      showOutline: false,
      outlineColor: "#8d949e",
      characterColor: "#001834",
      highlightOnComplete: true,
      drawingWidth: 16,
    });
    writer.hideCharacter();
  } else {
    writer.setCharacter(character);
  }

  startQuiz();
};

CardManager.onFsrsReady(updateHanziWriter);

$("#reveal-button").click(() => {
  writer.showCharacter();
  $("#reveal-button").hide();
  $("#hide-button").show();
  writer.cancelQuiz();
});
$("#hide-button").click(() => {
  writer.hideCharacter();
  $("#reveal-button").show();
  $("#hide-button").hide();
  startQuiz();
});
$("#animate-button").click(() => {
  writer.animateCharacter();
  $("#reveal-button").hide();
  $("#hide-button").show();
});
$("#erase-button").click(() => {
  writer.cancelQuiz();
  startQuiz();
});
$("#mute-button").click(() => {
  $("#mute-button").hide();
  $("#unmute-button").show();
  muted = true;
});
$("#unmute-button").click(() => {
  $("#mute-button").show();
  $("#unmute-button").hide();
  muted = false;
  if (!audio) {
    audio = new Audio("../sounds/Rise03.mp3");
  }
});
