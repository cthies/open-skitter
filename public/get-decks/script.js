var deckIndex =
  "https://gitlab.com/api/v4/projects/57289645/repository/files/index.json/raw?ref=master";

var deckSources = [];

var decks = [];

const getDeckSelector = (deckData) => {
  var title = deckData.title;
  var tags = deckData.tags.join(", ");
  var cards = deckData.cards;

  return `
    <div class="deckCard">
      <div class="deckDetails">
        <p class="deckTitle">${title}</p>
        <p class="deckSize">${cards.length} cards</p>
        <p class="deckTags">${tags}</p>
      </div>
      <div class="addDeck" id="addDeck-${title.replaceAll(" ", "")}">+</div>
      <div class="deckOwned" id="deckOwned-${title.replaceAll(" ", "")}">✓</div>
    </div>
  `;
};

const buildDeckHTML = () => {
  var deckHTML = decks
    .sort((a, b) => (a.title > b.title ? 1 : -1))
    .map((deck) => {
      var selector = getDeckSelector(deck);
      return selector;
    })
    .join("");
  $("#deckContainer").html(deckHTML);

  $(".addDeck").click((event) => {
    var key = $(event.target).attr("id");
    var title = key.split("-")[1];
    var toAdd = decks.filter(
      (deck) => deck.title.replaceAll(" ", "") === title
    )[0];
    toAdd.cards.forEach((card) => {
      card.deck = toAdd.title;
      CardManager.addCard(card);
    });
    updateDeckStatus();
  });
};

const updateDeckStatus = () => {
  var ownedDecks = CardManager.getDecks();
  ownedDecks.forEach((title) => {
    $("#addDeck-" + title.replaceAll(" ", "")).hide();
    $("#deckOwned-" + title.replaceAll(" ", "")).show();
  });
};

const showDeckOptions = () => {
  $.getJSON(deckIndex, (data) => (deckSources = data)).then(() => {
    deckSources = deckSources.map(
      (source) =>
        `https://gitlab.com/api/v4/projects/57289645/repository/files/${source}/raw?ref=master`
    );
    deckSources.map((source) =>
      $.getJSON(source, (data) => {
        decks.push(data);
      }).then(() => {
        buildDeckHTML();
        updateDeckStatus();
      })
    );
  });
};

CardManager.onFsrsReady(showDeckOptions);

console.log("found " + decks.length + " decks");
