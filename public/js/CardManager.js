class CardManager {
  static fsrs = undefined;
  static fsrsCallbackMethods = [];

  static setFsrs(fsrs) {
    CardManager.fsrs = fsrs;
    CardManager.fsrsCallbackMethods.forEach((fun) => fun());
  }

  static onFsrsReady(fun) {
    CardManager.fsrsCallbackMethods.push(fun);
  }

  static getAllCards() {
    var fsrs = CardManager.fsrs;
    if (!fsrs) return [];
    var cards = localStorage.getItem("cards");
    if (!cards) {
      return [];
    }
    cards = JSON.parse(cards);
    return cards.map((cardData) => {
      var card = fsrs.createEmptyCard();
      card.pinyin = cardData.pinyin;
      card.english = cardData.english;
      card.hanzi = cardData.hanzi;
      card.deck = cardData.deck;
      if (!cardData.difficulty) return card;
      card.difficulty = cardData.difficulty;
      card.due = Date.parse(cardData.due);
      card.elapsed_days = cardData.elapsed_days;
      card.lapses = cardData.lapses;
      card.last_review = cardData.last_review;
      card.reps = cardData.reps;
      card.scheduled_days = cardData.scheduled_days;
      card.stability = cardData.stability;
      card.stat = cardData.state;
      if (cardData.firstReview) {
        card.firstReview = cardData.firstReview;
      }
      return card;
    });
  }

  static setCards(cards) {
    localStorage.setItem("cards", JSON.stringify(cards));
  }

  static getDueCards(limit) {
    var allCards = CardManager.getAllCards();
    var addedToday = allCards.filter(
      (card) =>
        Date.parse(new Date()) - Date.parse(card.firstReview) <= 86400000
    ).length;
    var oldDueCards = allCards.filter(
      (card) => card.last_review && card.due <= new Date()
    );
    var newCards = allCards.filter((card) => !card.last_review);
    if (newCards.length > limit - addedToday) {
      newCards = newCards.slice(0, limit - addedToday);
    }
    return oldDueCards
      .concat(newCards)
      .sort((a, b) => (a.due > b.due ? 1 : -1));
  }

  static addCard(cardData) {
    var fsrs = CardManager.fsrs;
    if (!fsrs) return;
    var cards = CardManager.getAllCards();
    var newCard = fsrs.createEmptyCard();
    newCard.pinyin = cardData.pinyin;
    newCard.english = cardData.english;
    newCard.hanzi = cardData.hanzi;
    newCard.deck = cardData.deck;
    cards.push(newCard);
    CardManager.setCards(cards);
  }

  static getCardSchedules() {
    var fsrs = CardManager.fsrs;
    var FSRS = new fsrs.FSRS();
    if (!fsrs) return [];
    var cards = CardManager.getAllCards();
    return cards.map((card) => FSRS.repeat(card, new Date()));
  }

  static rateMistakes(mistakes) {
    if (mistakes == 0) {
      return 4;
    } else if (mistakes <= 2) {
      return 3;
    } else if (mistakes <= 4) {
      return 2;
    } else {
      return 1;
    }
  }

  static compareCards(a, b) {
    return (
      a.pinyin === b.pinyin &&
      a.hanzi === b.hanzi &&
      a.english === b.english &&
      a.deck === b.deck
    );
  }

  static saveCardReview(card, mistakes) {
    var fsrs = CardManager.fsrs;
    var FSRS = new fsrs.FSRS();
    if (!fsrs) return;
    var schedule = FSRS.repeat(card, new Date());
    var rating = CardManager.rateMistakes(mistakes);
    var newCard = schedule[rating].card;
    newCard.due = new Date(newCard.due);
    if (!newCard.firstReview) {
      newCard.firstReview = new Date();
    }
    var oldCards = CardManager.getAllCards();
    var newCards = oldCards.map((card) => {
      if (CardManager.compareCards(card, newCard)) {
        return newCard;
      } else {
        return card;
      }
    });
    CardManager.setCards(newCards);
  }

  static getDecks() {
    var cards = CardManager.getAllCards();
    var decks = cards.map((card) => card.deck);
    return [...new Set(decks)];
  }
}
